import {Row, Col, Container} from 'react-bootstrap';
import {useState, useEffect} from 'react';
import ShopCard from '../components/ShopCard';



export default function Marketplace({product}){

    const [products, setProducts] = useState([])

    useEffect(() => {
        fetch('https://mysterious-mesa-58916.herokuapp.com/products/viewAllProducts')
        .then(res => res.json())
        .then(data => {

            const productsArr = (data.map(product => {
                return(
                    <ShopCard key = {product._id} shopProp = {product} breakpoint={4}/>
                )
            }))

            setProducts(productsArr)
        })

    }, [products])


    return(
        <Container className="mb-4 pb-3">
            <Col>
                <Row className="mt-3 pt-4 shopView">
                    <h1 className="pb-4 text-center">Choose your new apple device </h1>
                    <hr className="h-line"/>
                </Row>
                <Row>
                {products}
                </Row>
            </Col>
        </Container>
    )
}