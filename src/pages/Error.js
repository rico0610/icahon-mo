import Banner from '../components/Banner';

export default function Error() {

    const data = {

        title: "404 - Not found",
        content: "The page you are looking for cannot be found",
        destination: "/",
        label: "Back home"
    }
    
    return (
        <>
        
        <div>
            <img style={{width: "100%"}} src="https://cdn.pixabay.com/photo/2016/03/08/01/21/hole-1243311_960_720.jpg" alt="error-img"></img>
            <Banner data={data}/>
        </div>
        </>
    )
}
